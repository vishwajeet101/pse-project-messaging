﻿//-----------------------------------------------------------------------
// <author> 
//     Amish Ranjan
// </author>
//
// <date> 
//     11-10-2018 
// </date>
// 
// <reviewer> 
//     Amish Ranjan
// </reviewer>
// 
// <copyright file="CompressionUnitTest.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This is a class intended to provide Test Cases for class Compression
//      and uses the ITest Interface. There are some saved images in specs directory
//      which are being used for testing the module. For testing, the same image is 
//      compressed and decompressed and then checked if they are same or not.
//      For log specifications refer to QualityAssurance.
// </summary>
//-----------------------------------------------------------------------
namespace Masti.ImageProcessing
{
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using Masti.QualityAssurance;

    /// <summary>
    /// This class provides test cases for compression class.
    /// </summary>
    public class CompressionUnitTest : ITest
    {
        /// <summary>
        /// The logger is used for Logging functionality within the test.
        /// Helps in debugging of tests.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="CompressionUnitTest" /> class.
        /// </summary>
        /// <param name="logger">Assigns the Logger to be used by the Test</param>
        public CompressionUnitTest(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Checks if the compression module works fine or not.
        /// It uses two images already stored and applies Compress
        /// and Decompress method on same and checks if the same image 
        /// is retrieved at the end.
        /// </summary>
        /// <returns>Returns whether the status is successful or not.</returns>
        public bool Run()
        {
            this.logger.LogInfo(this.LogHelper("ImageProcessing.CompressionUnitTest.Run: " +
                "Starting Test: Stay back this may take few seconds"));
            using (Compression testCompressor = new Compression())
            {
                using (Compression testDecompressor = new Compression())
                {
                    bool testWithDiff = this.TestUtility(true, testCompressor, testDecompressor);
                    bool testWithoutDiff = this.TestUtility(false, testCompressor, testDecompressor);
                    this.logger.LogSuccess(this.LogHelper("ImageProcessing.CompressionUnitTest.Run: " +
                        "Returning from Run"));
                    return testWithDiff && testWithoutDiff;
                }
            }
        }

        /// <summary>
        /// This method does the unit testing of this module when diff is enabled/disabled.
        /// </summary>
        /// <param name="implementDiff">Give true/false whether you want to implement 
        /// diff or not</param>
        /// <param name="testCompressor"> Compression object for client side </param>
        /// <param name="testDecompressor">Compression object for Server side </param>
        /// <returns>true/false for correct/incorrect functioning</returns>
        private bool TestUtility(bool implementDiff, Compression testCompressor, Compression testDecompressor)
        {
            Bitmap bmp1 = (Bitmap)Image.FromFile(@"../../../ImageProcessing/Specs/111501032Amish/1.png");
            Bitmap bmp2 = (Bitmap)Image.FromFile(@"../../../ImageProcessing/Specs/111501032Amish/2.png");

            Dictionary<int, Bitmap> bmp1Dict;

            ////For converting int and bool to string
            CultureInfo culture;
            culture = CultureInfo.CreateSpecificCulture("eu-ES");

            if (implementDiff == true)
            {
                testCompressor.CreatePrevBmp = true;
                bmp1Dict = testCompressor.Compress(bmp1, false);
                testCompressor.CreatePrevBmp = false;
            }
            else
            {
                bmp1Dict = testCompressor.Compress(bmp1, false);
            }

            if (bmp1Dict.ContainsKey(-2))
            {
                this.logger.LogError(this.LogHelper("ImageProcessing.CompressionUnitTest.TestUtility: " +
                    "Error in Compress method with implementDiff " + implementDiff.ToString(culture)));
                return false;
            }

            Bitmap bmp1Decompressed = testDecompressor.Decompress(bmp1Dict, false);
            if (bmp1Decompressed == null || !testCompressor.Equals(bmp1, bmp1Decompressed))
            {
                this.logger.LogError(this.LogHelper("ImageProcessing.CompressionUnitTest.TestUtility: " +
                    "Error in Decompress method with implementDiff " + implementDiff.ToString(culture)));
                return false;
            }

            if (implementDiff == true)
            {
                Dictionary<int, Bitmap> bmp2Dict = testCompressor.Compress(bmp2, true);
                if (bmp2Dict.ContainsKey(-2))
                {
                    this.logger.LogError(this.LogHelper("ImageProcessing.CompressionUnitTest." +
                        "TestUtility: Error in Compress method in implementing diff"));
                    return false;
                }

                Bitmap bmp2Decompressed = testDecompressor.Decompress(bmp2Dict, true);
                if (bmp2Decompressed == null || !testDecompressor.Equals(bmp2, bmp2Decompressed))
                {
                    this.logger.LogError(this.LogHelper("ImageProcessing.CompressionUnitTest." +
                        "TestUtility: Error in Decompress method in implementing diff"));
                    return false;
                }
            }
            else
            {
                Dictionary<int, Bitmap> bmp2Dict = testCompressor.Compress(bmp2, false);
                if (bmp2Dict.ContainsKey(-2))
                {
                    this.logger.LogError(this.LogHelper("ImageProcessing.CompressionUnitTest." +
                        "TestUtility: Error in Compress method without implementing diff"));
                    return false;
                }

                Bitmap bmp2Decompressed = testDecompressor.Decompress(bmp2Dict, false);
                if (bmp2Decompressed == null || !testDecompressor.Equals(bmp2, bmp2Decompressed))
                {
                    this.logger.LogError(this.LogHelper("ImageProcessing.CompressionUnitTest." +
                        "TestUtility: Error in Decompress method without implementing diff"));
                    return false;
                }
            }

            this.logger.LogSuccess(this.LogHelper("ImageProcessing.CompressionUnitTest.TestUtility: " +
                "Testing Successful with implementDiff " + implementDiff.ToString(culture)));
            return true;
        }

        /// <summary>
        /// helps to bypass log warning
        /// </summary>
        /// <param name="log">the value to be returned</param>
        /// <returns>same log value</returns>
        private string LogHelper(string log)
        {
            return log;
        }
    }
}
