﻿//-----------------------------------------------------------------------
// <author>
//      Polu Varshith
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Save and delete chat sessions.
// </summary>
// <copyright file="ServerPersistance.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Globalization;
    using System.Windows.Forms;
    using Masti.QualityAssurance;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Deletes session.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void DeleteSessionButtonClick(object sender, EventArgs e)
        {
            DialogResult confirmDeleteResult = MessageBox.Show("Do you really want to delete the sessions?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmDeleteResult.Equals(DialogResult.OK))
            {
                // Do something.
                int fromSessionID = int.Parse(fromSessionTextBox.Text, CultureInfo.InvariantCulture);
                int toSessionID = int.Parse(toSessionTextBox.Text, CultureInfo.InvariantCulture);
                this.messageHandler.DeleteMessage(fromSessionID, toSessionID);
                MastiDiagnostics.LogInfo("From: " + " To: " + " Sessions deleted.");
            }
        }

        /// <summary>
        /// Retrieve session.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void RetrieveSessionButtonClick(object sender, EventArgs e)
        {
            // Do something.
            int fromSessionID = int.Parse(fromSessionTextBox.Text, CultureInfo.InvariantCulture);
            int toSessionID = int.Parse(toSessionTextBox.Text, CultureInfo.InvariantCulture);
            this.messageHandler.RetrieveMessage(fromSessionID, toSessionID);

            string fileName = "User/NaaIshtam.txt";
            MastiDiagnostics.LogInfo("From: " + " To: " + " Sessions retreived to the following file." + fileName);
            MessageBox.Show("Sessions retreived to the following file." + fileName, "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
