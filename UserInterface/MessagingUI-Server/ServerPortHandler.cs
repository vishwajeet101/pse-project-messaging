﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      Polu Varshith
// </reviewer>
// <date>
//      11-Nov-2018
// </date>
// <summary>
//      Functionalities of UI design
// </summary>
// <copyright file="ServerPortHandler.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Globalization;
    using System.Windows.Forms;
    using Masti.ImageProcessing;
    using Masti.QualityAssurance;
    using Messenger;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Check if the port is available.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void PortButtonClick(object sender, EventArgs e)
        {
            bool isPortNumberValid = true;
            if (portTextBox.TextLength == 0)
            {
                MessageBox.Show("Please enter a port number", "OK", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }            

            this.serverPort = int.Parse(portTextBox.Text, CultureInfo.InvariantCulture);
            try
            {
                this.messageHandler = new Messager(this.serverPort);
            }
            catch (Exception exe)
            {
                isPortNumberValid = false;
                MessageBox.Show(exe.Message, "OK", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (isPortNumberValid)
            {
                //// Enable the chat section.
                shareScreenButton.Enabled = true;
                sendButton.Enabled = true;
                serverMessageTextBox.Enabled = true;
                portListenButton.Enabled = false;
                deleteSessionButton.Enabled = true;
                retrieveSessionButton.Enabled = true;
                fromSessionTextBox.Enabled = true;
                toSessionTextBox.Enabled = true;
            }
            else
            {
                MastiDiagnostics.LogError(portTextBox.Text + " Port number not available or is invalid");
                MessageBox.Show("Try a different port", "OK", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            { 
                this.imageHandler = FactoryImageServer.CreateImageProcessing(this.serverPort);
            }
            catch (Exception exe)
            {
                MessageBox.Show(exe.Message, "OK", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }  
            
            // Write the below code in try catch.
            this.messageHandler.SubscribeToDataReceiver(this.ReceiverMessageHandler);
            this.messageHandler.SubscribeToStatusReceiver(this.SendingStatusHandlers);
            this.messageHandler.SubscribeToConnectifier(this.EstablishConnection);
            this.imageHandler.RegisterImageUpdateHandler(this.ImageSubscriber);
        }
    }
}
