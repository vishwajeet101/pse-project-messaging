﻿//-----------------------------------------------------------------------
// <author> 
//    Vishwajeet
// </author>
//
// <date> 
//     11-oct-2018 
// </date>
// 
// <reviewer> 
//     Aryan Raj
// </reviewer>
// 
// <copyright file="MessagingForPersistenceTest.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      The following file contain our SchemaStub.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.Messenger.UnitTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.Messenger.Stubs;
    using Masti.QualityAssurance;

    /// <summary>
    /// class implementing Test for Persistence related function 
    /// </summary>
    public class MessagingForPersistenceTest : ITest
    {
        /// <summary>
        /// variable for logger
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Dummy dictionary for test
        /// </summary>
        private readonly Dictionary<string, string> dictionary = new Dictionary<string, string>()
        {
            { "type", "success" }
        };

        /// <summary>
        /// Variable for storing Messager Object
        /// </summary>
        private Messager messegerobj;

        /// <summary>
        /// Test var for delete status
        /// </summary>
        private int deleteStatus = 0;

        /// <summary>
        /// Test var for store status
        /// </summary>
        private bool storeStatus = false;

        /// <summary>
        /// Test var for retrieve status
        /// </summary>
        private string retrieveStatus = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagingForPersistenceTest"/> class.
        /// </summary>
        /// <param name="logger">logger for log</param>
        public MessagingForPersistenceTest(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Implementing Run 
        /// </summary>
        /// <returns>return the success or failure of the Run</returns>
        public bool Run()
        { 
            this.messegerobj = new Messager(8000)
            {
                PersistObj = new PersistenceStub()
            };
            
            this.deleteStatus = this.messegerobj.DeleteMessage(1, 4);
            
            this.storeStatus = this.messegerobj.StoreMessage(this.dictionary);

            this.retrieveStatus = this.messegerobj.RetrieveMessage(1, 4);
            if (this.storeStatus)
            {
                this.logger.LogSuccess("Successfully Stored Messages");
            }
            else
            {
                this.logger.LogError("Error in Storing Messages");
                return false;
            }
     
            if (this.deleteStatus == 1)
            {
                this.logger.LogSuccess("Successfully deleted Messages");
            }
            else
            {
                this.logger.LogError("Error in deleting Messages");
                return false;
            }

            if (string.IsNullOrEmpty(this.retrieveStatus))
            {
                this.logger.LogError("Error in retrieving Messages");
                return false;
            }
            else
            {
                this.logger.LogSuccess("Successfully retrieved Messages");   
            }

            return true;
        }
    }
}
