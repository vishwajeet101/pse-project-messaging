﻿//-----------------------------------------------------------------------
// <author> 
//    Anish M M (anishmathewdev@gmail.com)
// </author>
//
// <date> 
//     17th November, 2018
// </date>
// 
// <reviewer> 
//
// </reviewer>
// 
// <copyright file="SingletonTelemetryTest.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//    This class tests whether TelemetryCollection class follows singleton pattern.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.QualityAssurance.TelemetryTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class to test if TelemetryCollector class is a singleton.
    /// </summary>
    public class SingletonTelemetryTest : ITest
    {
        /// <summary>
        /// Logger instance used to log messages while testing.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref=SingletonTelemetryTest" /> class.
        /// </summary>
        /// <param name="logger">The logger instance to be used to log messages while testing.</param>
        public SingletonTelemetryTest(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Runs test to check if TelemetryCollection class works as singleton.
        /// </summary>
        /// <returns>Success status of tests run.</returns>
        public bool Run()
        {
            // Compare two TelemetryCollection instances. Must be same.
            this.logger.LogInfo("Getting two instances from TelemetryCollector class...");

            ITelemetryCollector telemetryCollector1 = TelemetryCollector.Instance;
            ITelemetryCollector telemetryCollector2 = TelemetryCollector.Instance;
            var isSingleton = false;
            
            // Object comparison checks if same object, not just same values.
            if (telemetryCollector1 == telemetryCollector2)
            {
                this.logger.LogSuccess("Both instances are same. TelemetryCollector is singleton. Got intended result.");
                isSingleton = true;
            }
            else
            {
                this.logger.LogWarning("Instances are different. Telemetry class is not following singleton design pattern. Not intended result!");
            }

            return isSingleton;
        }
    }
}