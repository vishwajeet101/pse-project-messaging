//-----------------------------------------------------------------------
// <author> 
//     Automatically generated
// </author>
//
// <date> 
//     25/10/2018
// </date>
// 
// <reviewer>
//      None
// </reviewer>
// 
// <copyright file="GlobalSuppressions.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      None
// </summary>
//-----------------------------------------------------------------------

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", Justification = "_id is required as a field by MongoDB", MessageId = "id", Scope = "member", Target = "Masti.Persistence.Document.#_id")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "_id is required as a field by MongoDB", Scope = "member", Target = "~P:Masti.Persistence.Document._id")]// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.
